###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Christopher Aguilar <ciaguila@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   Feb 2021
###############################################################################

CC     = gcc
CFLAGS = -g

TARGET = animalfarm

all: $(TARGET)

main.o: main.c cat.h
	$(CC) $(CFLAGS) -c main.c

animals.o: animals.c animals.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.c cat.h
	$(CC) $(CFLAGS) -c cat.c


animalfarm: main.o cat.o animals.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o cat.o animals.o


clean:
	rm -f *.o


