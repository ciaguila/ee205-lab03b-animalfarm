///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"
char *ColorName(enum Color c)
{
    char *strings[] = { "black", "white", "red", "blue", "green", "pink" };

    return strings[c];
}


char *GenderName(enum Gender g)
{
    char *strings[] = { "male", "female" };

    return strings[g];
}


char *BreedName(enum Breed b)
{
    char *strings[] = { "main coon", "manx", "shorthair", "persian", "sphynx"};

    return strings[b];
}

/// Decode the enum Color into strings for printf()
// char* colorName (enum Color color) {
   

//   return NULL; // We should never get here
// };


